#include <iostream>
#include <tuple>
#include <cmath> 
//#include <math.h>

using std::cout;
using std::endl;


class MyOwnClass //first part of HW
{
public:
    MyOwnClass():speed(100),count(4)
    {}

    std::tuple<int, float> Show_MyProperties()
    {
        return std::make_tuple(count, speed);
    }

private:
    float speed;
    int count;

};



class Vector
{
public:
    Vector() :x(1), y(1), z(1)
    {}

    Vector(double _x, double _y, double _z) :x(_x), y(_y), z(_z)
    {}

    void Show_direction() {
        cout << "x: " << x << "/ y: " << y << "/ z: "<< z << endl;
    }

    double Get_VectorLenght() // Fuction for counting of vector lenght
    {
        double PoweredLenght = pow(x,2) + pow(y,2) + pow(z,2);
        return sqrt(PoweredLenght);


    }


private:
    double x;
    double y;
    double z;
};



int main()
{
    cout << "Hello World!\n";

    MyOwnClass firstObj;

    auto Count_Speed = firstObj.Show_MyProperties();
    cout <<"Count: "<<std::get<0>(Count_Speed) <<endl<<"Speed: "<<std::get<1>(Count_Speed)<<endl;


    Vector firstObj_direction(1, 20, 100);
    firstObj_direction.Show_direction();

    cout <<"The Lenght of your direction vector is ____ " <<firstObj_direction.Get_VectorLenght();

    return 0;
}

